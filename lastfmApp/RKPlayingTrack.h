//
//  RKPlayingTrack.h
//  lastfmApp
//
//  Created by alexander.oschepkov on 15/08/17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RKPlayingTrack : NSObject

@property (nonatomic, copy) NSString *mbid;
@property (nonatomic, copy) NSString *artist;
@property (nonatomic, copy) NSString *track;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSArray *genres;
@property (nonatomic, copy) NSDate *date;

@end
