//
//  ViewController.h
//  lastfmApp
//
//  Created by alexander.oschepkov on 15/08/17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

