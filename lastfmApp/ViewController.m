//
//  ViewController.m
//  lastfmApp
//
//  Created by alexander.oschepkov on 15/08/17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <RestKit/RestKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ViewController.h"
#import "RKPlayingTrack.h"
#import <PromiseKit/PromiseKit.h>

#define API_KEY @"23aa88b22fbf8172701405ee1dbc323c"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;

@property (strong, nonatomic) NSMutableArray *tracksArray;
@property (assign, nonatomic) NSInteger pageNum;

@end

@implementation ViewController

static NSInteger tracksPerPage = 20;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageNum = 1;
    self.tracksArray = [NSMutableArray array];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (NSString *)dateToRelative:(NSDate *)trackDate {
    NSDateComponentsFormatter *formatter = [[NSDateComponentsFormatter alloc] init];
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleFull;
    formatter.allowedUnits = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSString *dateString = [formatter stringFromDate:trackDate toDate:[NSDate date]];
    return [[[dateString componentsSeparatedByString:@","] firstObject] stringByAppendingString:@" ago"];
}

- (void)recentTracksOfUser:(NSString *)username isAppending:(BOOL)isAppending {
    __weak ViewController *weakSelf = self;
    
    [self getTracksOfUser:username isAppending:isAppending].then(^(NSArray *array) {
        return [weakSelf getGenresForRecentTracks:array isAppending:isAppending];
    }).then(^(NSMutableArray *array) {
        if(isAppending) {
            [weakSelf.tracksArray addObjectsFromArray:array];
        } else {
            weakSelf.tracksArray = (NSMutableArray *)array;
        }
        
        weakSelf.pageNum++;
        [weakSelf.tableView reloadData];
    });
}

- (AnyPromise *)getTracksOfUser:(NSString *)username isAppending:(BOOL)isAppending {
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[RKPlayingTrack class]];
        [mapping addAttributeMappingsFromDictionary:@{
                                                      @"mbid": @"mbid",
                                                      @"artist.#text": @"artist",
                                                      @"name": @"track",
                                                      @"image.#text.@firstObject": @"cover",
                                                      @"date.uts": @"date"
                                                      }];
        
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:nil keyPath:@"recenttracks.track" statusCodes:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=%@&api_key=%@&format=json&limit=%ld&page=%ld", username, API_KEY, tracksPerPage, self.pageNum]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
        
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            resolve([result array]);
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", [error localizedDescription]);
        }];
        [operation start];
    }];
}

- (AnyPromise *)getGenresForRecentTracks:(NSArray *)tracksArray isAppending:(BOOL)isAppending {
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        AnyPromise *trackPromise = [AnyPromise promiseWithValue: nil];
        
        for(RKPlayingTrack *track in tracksArray) {
            trackPromise = trackPromise.then(^{
                return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                    NSString *formatedArtist = [track.artist stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=%@&api_key=%@&format=json", formatedArtist, API_KEY]]];
                    [request setHTTPMethod:@"GET"];
                    
                    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if(data != nil) {
                            NSDictionary* responseJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                            
                            NSMutableArray *genresArray = [NSMutableArray array];
                            
                            [[[[responseJSON objectForKey:@"artist"] objectForKey:@"tags"] objectForKey:@"tag"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                [genresArray addObject:(NSDictionary *)[obj objectForKey:@"name"]];
                            }];
                            
                            track.genres = genresArray;
                            resolve(genresArray);
                        }
                    }] resume];
                }];
            });
        }
        
        trackPromise.then(^{
            resolve(tracksArray);
        });
    }];
}

- (IBAction)userFieldChanged:(UITextField *)sender {
    self.pageNum = 1;
    [self recentTracksOfUser:[sender.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isAppending:NO];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tracksArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Track";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    RKPlayingTrack *track = [self.tracksArray objectAtIndex:indexPath.row];
    
    __weak UITableViewCell *weakCell = cell;
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:track.cover] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        weakCell.imageView.image = image;
        [weakCell layoutSubviews];
    }];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", track.artist, track.track];
    
    // Placed artist tags instead time
    //cell.detailTextLabel.text = [self dateToRelative:track.date];
    cell.detailTextLabel.text = [track.genres componentsJoinedByString:@", "];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)aScrollView willDecelerate:(BOOL)decelerate {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -250;
    if(y > h + reload_distance) {
        [self recentTracksOfUser:self.searchField.text isAppending:YES];
    }
}

@end
